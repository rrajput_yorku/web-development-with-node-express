var express = require("express");

var app = express();

//set up handlebars view engine - this will create a view engine and configures express to use it by default.
var handlebars = require("express3-handlebars").create({
  defaultLayout: "main",
});
app.engine("handlebars", handlebars.engine);
app.set("view engine", "handlebars");

app.set("port", process.env.PORT || 3000);

app.use(express.static(__dirname + "/public"));

app.get("/", (req, res) => {
  res.render("home");
});

app.get("/about", (req, res) => {
  let randomFortune = fortunes[Math.floor(Math.random() * fortunes.length)];
  res.render("about", { fortune: randomFortune });
});

//custom 404 page - catch all handlers (middleware)
app.use(function (req, res, next) {
  res.status(404);
  res.render("404");
});

//custom 500 page - 500 error handler (middleware)
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500);
  res.render("500");
});

app.listen(app.get("port"), function () {
  console.log(
    "Express started on http://localhost:" +
      app.get("port") +
      " \nPress Ctrl + C to terminate..."
  );
});

let fortunes = [
  "Conquer your fears or they will conquer you",
  "Rivers need spring",
  "Do not fear what you don't know",
  "You will have a pleasant surprise",
  "Whenever possible, keep it simple",
];
